package pcd.ass03.chat;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class ChatUserMain {
	public static void main(String[] args) {
		  String nickname = "joe"; 
		  String localPort = "2555";
		  String registryPort = "2552";
	
		  String systemName = "Chat";
		  String registryHost = "127.0.0.1";
		  
		  if (args.length > 0) {
			  nickname = args[0];
			  localPort = args[1];
			  registryPort = args[2];
		  }

		  System.out.println(".:: CHAT USER "+nickname+" - server port: "+registryPort+" local port: "+localPort+" ::.");

		  Config conf = ConfigFactory.parseString(makeConfig(localPort));
		  ActorSystem system = ActorSystem.create(systemName, conf);
		  
		  String registryPath = "akka.tcp://"+systemName+"@"+registryHost+":"+registryPort+"/user/registry";
		  system.actorOf(Props.create(ChatUserActor.class, nickname, registryPath));
  	}

	static private String makeConfig(String port) {
		return 
				"akka {\n" + 
				"  actor {\n" + "    provider = remote\n" + "  }\n" + 
				"  remote {\n"+ 
				"    enabled-transports = [\"akka.remote.netty.tcp\"]\n" + 
				"    netty.tcp {\n"+ 
				"      hostname = \"127.0.0.1\"\n" + 
				"      port = " + port + "\n" + 
				"    }\n" + 
				" }\n" + 
				"}";
	}
  
}

package pcd.ass03.chat.msgs;

import java.io.Serializable;

public class ChatUserKeepAliveReplyMsg implements Serializable {

	private String nickname;
	
	public ChatUserKeepAliveReplyMsg(String nickname){
		this.nickname = nickname;
	}
	
	public String getNickname(){
		return nickname;
	}
}

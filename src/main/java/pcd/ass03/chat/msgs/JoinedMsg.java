package pcd.ass03.chat.msgs;

import java.io.Serializable;

public class JoinedMsg implements Serializable {

	private String brokerPath;
	
	public JoinedMsg(String brokerPath) {
		this.brokerPath = brokerPath;
	}
	
	public String getBrokerPath() {
		return brokerPath;
	}
}

package pcd.ass03.chat.msgs;

import java.io.Serializable;

public class DispatchChatMsg implements Serializable {

	private String msg;
	private String senderNickname;
	
	public DispatchChatMsg(String msg, String senderNickname){
		this.msg = msg;
		this.senderNickname = senderNickname;
	}
	
	public String getMsg(){
		return msg;
	}

	public String getSenderNickname(){
		return senderNickname;
	}
}

package pcd.ass03.chat.msgs;

import java.io.Serializable;

import akka.actor.ActorRef;

public class RemoveParticipantMsg implements Serializable {

	private String nickname;
	
	public RemoveParticipantMsg(String nickname){
		this.nickname = nickname;
	}
	
	public String getNickname(){
		return nickname;
	}
	
}

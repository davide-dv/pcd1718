package pcd.ass03.chat.msgs;

import java.io.Serializable;

import akka.actor.ActorRef;

public class ExitCSMsg implements Serializable {

	private String nickname;
	
	public ExitCSMsg(String nickname){
		this.nickname = nickname;
	}
	
	public String getNickname(){
		return nickname;
	}
	
}

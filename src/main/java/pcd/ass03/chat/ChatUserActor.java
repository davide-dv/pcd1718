package pcd.ass03.chat;

import java.time.Duration;

import akka.actor.*;
import pcd.ass03.chat.msgs.ChatMsg;
import pcd.ass03.chat.msgs.ChatUserKeepAliveMsg;
import pcd.ass03.chat.msgs.ChatUserKeepAliveReplyMsg;
import pcd.ass03.chat.msgs.DispatchChatMsg;
import pcd.ass03.chat.msgs.EnterCSCmdMsg;
import pcd.ass03.chat.msgs.EnterCSMsg;
import pcd.ass03.chat.msgs.ExitCSCmdMsg;
import pcd.ass03.chat.msgs.ExitCSMsg;
import pcd.ass03.chat.msgs.InputMsg;
import pcd.ass03.chat.msgs.JoinMsg;
import pcd.ass03.chat.msgs.JoinedMsg;
import pcd.ass03.chat.msgs.NewChatUserMsg;
import pcd.ass03.chat.msgs.QuitCmdMsg;
import pcd.ass03.chat.msgs.QuitMsg;
import pcd.ass03.chat.msgs.UserEnteredInCSMsg;
import pcd.ass03.chat.msgs.UserExitCSMsg;
import pcd.ass03.chat.msgs.UserForcedExitCSMsg;

public class ChatUserActor extends AbstractActorWithTimers {

	private ActorSelection reg;
	private ActorSelection broker;
	private String nickname;

	private static Object TIMER = "timer";
	private static final long REG_TIMEOUT = 2000;
	
	private static final long CHAT_WATCHDOG_TIMEOUT = 6000;	
	private long lastKeepaliveTimestamp;
	
	private AbstractActor.Receive chattingBehaviour;
	private AbstractActor.Receive joiningBehaviour;

	public ChatUserActor(String nickname, String registerPath) {
		this.reg = getContext().actorSelection(registerPath);
		this.nickname = nickname;
	}

	public void preStart() {		
		log("Contacting the register at " + reg.pathString() + "...");
		reg.tell(new JoinMsg(getSelf(), nickname), getSelf());
		getTimers().startSingleTimer(TIMER, new RegistryTimeoutEvent(), Duration.ofMillis(REG_TIMEOUT));
	}

	@Override
	public Receive createReceive() {
		chattingBehaviour = receiveBuilder().match(InputMsg.class, msg -> {
			broker.tell(new DispatchChatMsg(nickname,msg.getMsg()), getSelf());
		}).match(ChatMsg.class, msg -> {
			logChat(msg.getSenderNickname(), msg.getMsg());
		}).match(QuitMsg.class, msg -> {
			logSystem(msg.getNickname()+" left the chat.");
		}).match(NewChatUserMsg.class, msg -> {
			logSystem(msg.getNickname()+" entered the chat.");
		}).match(UserEnteredInCSMsg.class, msg -> {
			logSystem(msg.getNickname() + " is starting a soliloquium.");
		}).match(UserExitCSMsg.class, msg -> {
			logSystem(msg.getNickname() + " completed the soliloquium.");
		}).match(UserForcedExitCSMsg.class, msg -> {
			logSystem(msg.getNickname() + " forced to complete the soliloquium.");
		}).match(QuitCmdMsg.class, msg -> {
			reg.tell(new QuitMsg(nickname), getSelf());
			logSystem("quit.");
			// getContext().stop(getSelf());
			System.exit(0);
		}).match(EnterCSCmdMsg.class, msg -> {
			broker.tell(new EnterCSMsg(nickname), getSelf());
		}).match(ExitCSCmdMsg.class, msg -> {
			broker.tell(new ExitCSMsg(nickname), getSelf());
		}).match(ChatUserKeepAliveMsg.class, msg -> {
			// log("keep alive received - replying.");
			lastKeepaliveTimestamp = System.currentTimeMillis();
			getSender().tell(new ChatUserKeepAliveReplyMsg(nickname), getSelf());
		}).match(ChatWatchdogTimeoutEvent.class, msg -> {
			// log("check chat availability watchdog");
			if (System.currentTimeMillis() - lastKeepaliveTimestamp > CHAT_WATCHDOG_TIMEOUT) {
				logSystem("Chat no more available, going to quit.");
				System.exit(0);
			}
		}).build();

		joiningBehaviour = receiveBuilder().match(JoinedMsg.class, msg -> {
			logSystem("Joined.");
			this.broker = getContext().actorSelection(msg.getBrokerPath());
			new ConsoleAgent(getSelf()).start();
			getTimers().startPeriodicTimer(TIMER, new ChatWatchdogTimeoutEvent(), Duration.ofMillis(CHAT_WATCHDOG_TIMEOUT));
			getContext().become(chattingBehaviour);
		}).match(RegistryTimeoutEvent.class, msg -> {
			logSystem("Chat not available. Shutdown. ");
			// getContext().stop(getSelf());
			System.exit(0);
		}).build();

		return joiningBehaviour;
	}

	private void logChat(String who, String msg) {
		synchronized (System.out) {
			System.out.println("[" + who + "] " + msg);
		}
	}

	private void logSystem(String msg) {
		synchronized (System.out) {
			System.out.println("[ system ] " + msg);
		}
	}

	private void log(String msg) {
		synchronized (System.out) {
			System.out.println("[ system ] " + msg);
		}
	}

}

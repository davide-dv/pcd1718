package pcd.ass03.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import akka.actor.ActorRef;
import pcd.ass03.chat.msgs.*;

public class ConsoleAgent extends Thread {

	private ActorRef userActor;
	private final String QUIT_CMD = ":quit";
	private final String ENTERCS_CMD = ":enter-cs";
	private final String EXITCS_CMD = ":exit-cs";
	
	public ConsoleAgent(ActorRef ref) {
		userActor = ref;
	}
	public void run() {
		logUI(".:: CHAT CONSOLE ::.");
		logUI("Type your messages.");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		boolean quit = false;
		while (!quit) {
			try {
				String msg = br.readLine().trim();
				if (!msg.equals("")) {
					if (msg.equals(QUIT_CMD)) {
						userActor.tell(new QuitCmdMsg(), ActorRef.noSender());
						log("sent quit.");
						quit = true;
					} else if (msg.equals(ENTERCS_CMD)){
						userActor.tell(new EnterCSCmdMsg(), ActorRef.noSender());					
						log("sent enter cs");
					} else if (msg.equals(EXITCS_CMD)) {
						userActor.tell(new ExitCSCmdMsg(), ActorRef.noSender());
						log("sent exit cs");
					} else {
						/* normal chat msg */
						userActor.tell(new InputMsg(msg), ActorRef.noSender());
					}					
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		logUI(":: BYE BYE ::");
	}
	
	private void log(String msg) {
		synchronized (System.out) {
			// System.out.println("[ console ] "+msg);
		}
	}

	private void logUI(String msg) {
		synchronized (System.out) {
			System.out.println(msg);
		}
	}
}

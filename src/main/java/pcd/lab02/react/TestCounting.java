package pcd.lab02.react;

public class TestCounting {
	public static void main(String[] args) {
		Counter c = new Counter(0);
        javax.swing.SwingUtilities.invokeLater(() -> {
        		new CounterGUI(c).setVisible(true);
        });
	}
}

package pcd.lab07.rx;

import io.reactivex.*;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.AsyncSubject;

public class Test3coldhot {

	public static void main(String[] args){
		
		// cold 
		
		Observable<Integer> source = Observable.create(subscriber -> {
	        for (int i = 0; i <= 2; i++) {
	            // System.out.println("source: " + i);
	            subscriber.onNext(i);
	        }
	        subscriber.onComplete();
	    });

	    source.subscribe(v -> System.out.println("A: "+v));
	    source.subscribe(v -> System.out.println("B: "+v));
	
	    /*
	    
	    Hot Observables

	    - don’t wait for any subscription. They start emitting items when created.
	    - don’t emit the sequence of items again for a new subscriber.
	    - When an item is emitted by hot observable, all the subscribers that are subscribed will get the emitted item at once.
	    
	    */
	    System.out.println("(Pseudo-) HOT observable");
	    
		Observable<Integer> cold = Observable.create(subscriber -> {
	        for (int i = 0; i <= 5; i++) {
	            // System.out.println("source: " + i);
	            subscriber.onNext(i);
	        }
	        subscriber.onComplete();
	    });
	    ConnectableObservable<Integer> hot = cold.publish();
	    hot.subscribe(v -> System.out.println("A: "+v));
	    hot.subscribe(v -> System.out.println("B: "+v));
	    // System.out.println("HEREHERE");
	    hot.connect();
	 
	    // HOT observable using Subject 
	    
	    System.out.println("(Pseudo-) HOT observable using Subject");
	    
	    Observable<Integer> source2 = Observable.create(subscriber -> {
	        for (int i = 0; i <= 10; i++) {
	            // System.out.println("Source Emits : " + i);
	            subscriber.onNext(i);
	            try {
	            		Thread.sleep(1000);
	            } catch (Exception ex) {
	            }
	        }
	        subscriber.onComplete();
	    });

	    AsyncSubject<Integer> asyncSubject = AsyncSubject.create();
	    source2.subscribe(asyncSubject);
	    asyncSubject.subscribe(v -> System.out.println("A: "+v));
	    asyncSubject.subscribe(v -> System.out.println("B: "+v));
	    
	}
}

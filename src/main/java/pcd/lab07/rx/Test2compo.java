package pcd.lab07.rx;

import java.util.*;

import io.reactivex.Observable;

public class Test2compo {

	public static void main(String[] args) {

		List<String> words = Arrays.asList(
				 "the",
				 "quick",
				 "brown",
				 "fox",
				 "jumped",
				 "over",
				 "the",
				 "lazy",
				 "dog"
				);
		
		Observable
		.fromIterable(words)
		.zipWith(Observable.range(1, 5), (string, count) -> String.format("%2d. %s", count, string))
		.subscribe(System.out::println);
		
	}

}

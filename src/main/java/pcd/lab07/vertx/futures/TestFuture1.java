package pcd.lab07.vertx.futures;
import io.vertx.core.*;


public class TestFuture1 {
	public static void main(String[] args) {
		
		Vertx vertx = Vertx.vertx();
		System.out.println("pre");
		Future<Void> fut = Future.future();
		fut.setHandler(ar -> {
			System.out.println("done");
		});
		vertx.setTimer(1000, res -> {
			fut.complete();
		});
		/*
		try {
			Thread.sleep(2000);
		} catch (Exception ex) {}
		*/
		System.out.println("post");
	}

}


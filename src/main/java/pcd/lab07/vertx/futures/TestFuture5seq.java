package pcd.lab07.vertx.futures;

import io.vertx.core.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

public class TestFuture5seq {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		FileSystem fs = vertx.fileSystem();   
		
		Future<Void> failFuture = Future.future();

		Future<Buffer> f1 = Future.future();
		fs.readFile("build.gradle", f1.completer());
		f1.compose(res -> {
			System.out.println("BUILD => \n"+res.toString().substring(0,160));
			Future<Buffer> f2 = Future.future();
			fs.readFile("settings.gradle", f2.completer());
			return f2;
		}).compose(res -> {
			System.out.println("SETTINGS => \n"+res.toString().substring(0,160));
		}, failFuture);
		
		failFuture.setHandler(res -> {
			System.out.println("Something faiiled.");
		});
	}

}


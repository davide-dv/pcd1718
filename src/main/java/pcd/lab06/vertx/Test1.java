package pcd.lab06.vertx;

import io.vertx.core.Vertx;

public class Test1 {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		vertx.createHttpServer().requestHandler(req -> {      
		      System.out.println("request arrived.");          
		      String fileName = req.path().substring(1);      
		      System.out.println(fileName);
		      
		      vertx.fileSystem().readFile(fileName, result -> {    	  
		        System.out.println("result ready");
		    	    if (result.succeeded()) {
		    	        System.out.println(result.result());
		    	        req.response()
		    	        	.putHeader("content-type", "text/plain")
		    	        	.end(result.result().toString());
		    	        
		    	    } else {
		    	        System.err.println("Oh oh ..." + result.cause());
		    	        req.response()
			        	.putHeader("content-type", "text/plain")
			        	.end("File not found");    	    
		    	    }
		      });     
		    }).listen(8081);
	}

}

package pcd.lab03.lost_updates.sol2;

public class UnsafeCounter {

	private int cont;
	
	public UnsafeCounter(int base){
		this.cont = base;
	}
	
	public synchronized void inc(){
		cont++;
	}
	
	public int getValue(){
		return cont;
	}
}

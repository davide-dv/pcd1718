package pcd.lab03.sync2;

public interface CounterObserver {

	void counterUpdated(int value);
	
	void counterClosed();
}

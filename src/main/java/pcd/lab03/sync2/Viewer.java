package pcd.lab03.sync2;

import java.util.concurrent.Semaphore;

public class Viewer extends Thread implements CounterObserver {

	private final static int POISON_PILL = -1;
	private BoundedBuffer events;
	private Output output;
	private Semaphore shutdown;
	
	public Viewer(Counter counter, Output output, Semaphore shutdown){
		super("Viewer");
		this.output = output;
		this.shutdown = shutdown;
		events = new BoundedBuffer(10);
		counter.addObserver(this);
	}
	
	public void run(){
		try {
			while (true){
				int value = events.take();
				if (value != POISON_PILL){
					output.print(""+value);
				} else {
					break;
				}
			}
			output.print("Goodbye");
			shutdown.release();
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void counterUpdated(int value){
		try {
			events.put(value);
		} catch (Exception ex){			
		}
	}

	public void counterClosed(){
		try {
			events.put(POISON_PILL);
		} catch (Exception ex){
		}
	}
	
}

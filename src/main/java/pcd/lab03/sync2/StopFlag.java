package pcd.lab03.sync2;

import java.util.concurrent.*;

/**
 * Stop flag
 * 
 * It is thread-safe because operations on the boolean data type
 * are atomic
 * 
 * @author aricci
 *
 */
public class StopFlag {

	private volatile boolean isSet;
	
	public StopFlag(){
		isSet = false;
	}
	
	public void set(){
		isSet = true;
	}
	
	public boolean isSet(){
		return isSet;
	}
}

package pcd.lab03.sync2;

import java.util.concurrent.Semaphore;

public class PingPong {

	public static void main(String[] args) {
		
		Semaphore shutdown = new Semaphore(0);
		
		Controller controller = new Controller(shutdown);
		new View(controller).setVisible(true);
		
		try {
			/* waiting for pinger, ponger, viewer */
			shutdown.acquire();
			shutdown.acquire();
			shutdown.acquire();
			System.exit(0);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		
	}

}

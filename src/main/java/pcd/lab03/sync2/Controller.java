package pcd.lab03.sync2;

import java.util.concurrent.Semaphore;

public class Controller {

	private Counter counter;
	private Output output;
	private StopFlag stopFlag;
	private Semaphore canGoPinger, canGoPonger;
	
	public Controller(Semaphore shutdown){
		counter = new Counter(0);
		output = new Output();
		stopFlag = new StopFlag();
		canGoPinger = new Semaphore(0);
		canGoPonger = new Semaphore(0);
		new Viewer(counter, output, shutdown).start();
		new Pinger(counter, canGoPinger, canGoPonger, stopFlag, output, shutdown).start();
		new Ponger(counter, canGoPonger, canGoPinger, stopFlag, output, shutdown).start();
	}

	public void notifiedStart(){
		canGoPinger.release();
	}
	
	public void notifiedStop(){
		stopFlag.set();
		canGoPinger.release();
		canGoPonger.release();
	}
}

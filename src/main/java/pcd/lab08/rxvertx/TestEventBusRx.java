package pcd.lab08.rxvertx;

import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.RxHelper;
import io.vertx.core.Vertx;
import io.vertx.rxjava.core.eventbus.EventBus;
import io.vertx.rxjava.core.eventbus.Message;
import io.vertx.rxjava.core.eventbus.MessageConsumer;
import rx.Observable;
import rx.Subscription;

public class TestEventBusRx {

	static class MyVerticle extends AbstractVerticle {

		private String channel;

		public MyVerticle(String channel) {
			this.channel = channel;
		}

		public void start() {
			System.out.println("Verticle started.");
			EventBus eb = vertx.eventBus();
			MessageConsumer<Object> consumer = eb.consumer(channel);
			Observable<Message<Object>> observable = consumer.toObservable();
			Subscription sub = observable.subscribe(event -> {
				System.out.println("perceived: " + event);
			});

			// Unregisters the stream after 10 seconds
			vertx.setTimer(10000, id -> {
				sub.unsubscribe();
			});
		}
	}

	public static void main(String[] args) throws Exception {
		io.vertx.rxjava.core.Vertx rxvertx = io.vertx.rxjava.core.Vertx.vertx();

		String channelName = "an-event-channel";

		MyVerticle myVerticle = new MyVerticle(channelName);
		RxHelper.deployVerticle(rxvertx, myVerticle);

		Thread.sleep(1000);
		EventBus eb = rxvertx.eventBus();
		eb.publish("an-event-channel", "MSG #1");
		Thread.sleep(1000);
		eb.publish("an-event-channel", "MSG #2");
	}

}

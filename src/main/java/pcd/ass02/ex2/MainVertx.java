package pcd.ass02.ex2;

import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import pcd.ass02.model.DataPiece;
import pcd.ass02.model.TaskData;

import java.util.regex.Pattern;

public class MainVertx
{
    public static void main(String[] args){
        Vertx vertx = Vertx.vertx();
        FlowableProcessor<DataPiece> flow = PublishProcessor.<DataPiece>create().toSerialized();
        flow.subscribe(d -> {
           if(d.getCount()>0) System.out.println(d.getPath() + " -> " + d.getCount());
        });
        TaskData data = new TaskData("/Users/roby/repos/exercises", 10, Pattern.compile("String\\[\\] args"));
        vertx.deployVerticle(new VerticleFileAnalyzer(data));
        vertx.deployVerticle(new VerticleFileSearcher(data));
    }
}

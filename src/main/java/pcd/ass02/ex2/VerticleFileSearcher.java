package pcd.ass02.ex2;

import static pcd.ass02.ex2.Channels.*;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.FileSystem;
import pcd.ass02.model.DataPiece;
import pcd.ass02.model.TaskData;
import pcd.ass02.utils.K;
import pcd.ass02.utils.Utils;

public class VerticleFileSearcher extends AbstractVerticle {
    private TaskData data;

    public VerticleFileSearcher(TaskData data){
        this.data = data;
    }

    @Override public void start(){
        EventBus eb = vertx.eventBus();

        eb.consumer(CHANNEL_DIRECTORY, dirTask -> {
            String[] dirData = dirTask.body().toString().split(":");
            String dir = dirData[0];
            int depth = Integer.parseInt(dirData[1]);
            //System.out.println("Process dir: " + dir);
            traverse(dir, depth);
        });

        traverse(data.getBasePath(), data.getDepth());
    }

    long currTime;

    public FileSystem traverse(String basePath, int depth){
        return vertx.fileSystem().readDir(basePath, files -> {
            VertxUtils.HandleError(files);
            if(files.succeeded()){
                files.result().forEach(file -> {
                    vertx.fileSystem().props(file, props -> {
                        VertxUtils.HandleError(props);
                        if(props.result().isDirectory() && depth-1>0) vertx.eventBus().send(CHANNEL_DIRECTORY, file+":"+(depth-1));
                        if(props.result().isRegularFile()) { vertx.eventBus().send(CHANNEL_END, 1); vertx.eventBus().send(CHANNEL_FILE, file); }
                        long thisVeryCurrTime = System.nanoTime();
                        currTime = thisVeryCurrTime;
                    });
                });
            } else {
                System.err.println("Cannot read directory " + basePath);
            }
        });
    }

    @Override public void stop(){ System.out.println("Stopped VerticleFileSearcher"); }
}

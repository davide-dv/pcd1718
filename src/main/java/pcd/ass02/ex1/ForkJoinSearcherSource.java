package pcd.ass02.ex1;

import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import pcd.ass02.model.DataPiece;
import pcd.ass02.model.TaskData;
import pcd.ass02.model.TaskResult;
import pcd.ass02.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Observable;
import java.util.concurrent.RecursiveTask;

public class ForkJoinSearcherSource extends RecursiveTask<Integer> {
    public static enum Kind { Base, Files };

    private TaskData taskData;
    private FlowableProcessor<DataPiece> subject;
    private File[] files;
    private int from;
    private int to;
    private Kind kind;

    public ForkJoinSearcherSource(TaskData taskData, FlowableProcessor<DataPiece> subject){
        this.taskData = taskData; // for main task
        this.subject = subject;
        kind = Kind.Base;
        //System.out.println("ForkJoin BASE TASK");
    }

    public ForkJoinSearcherSource(File[] files, int from, int to, TaskData taskData, FlowableProcessor<DataPiece> subject){
        this.files = files;
        this.from = from;
        this.to = to;
        this.taskData = taskData;
        this.subject = subject;
        kind = Kind.Files;
        //System.out.println("ForkJoin SUBTASK FROM " + from + " TO " + to);
    }

    @Override
    protected Integer compute() {
        if(kind == Kind.Base){
            File[] files = Utils.getFiles(taskData.getBasePath(), taskData.getDepth()).toArray(new File[]{});
            this.from = 0;
            this.to = files.length;
            spawn(files);

            Flowable<Integer> flow = Flowable.range(0, Integer.MAX_VALUE);
            subject.zipWith(flow, (d,k) -> k).filter(k -> k==files.length).subscribe(x -> subject.onComplete());
            return files.length;
        } else if(kind == Kind.Files && (to-from) > 1){
            spawn(files);
        }
        else if(kind == Kind.Files && (to-from)==1){
            try {
                String content = new String(Files.readAllBytes(files[from].toPath()));
                int numMatches = Utils.numMatches(taskData.getRegex(), content);
                TaskResult res = new TaskResult();
                res.put(files[from].toPath().toString(), numMatches);
                subject.onNext(new DataPiece(files[from].toPath().toString(), numMatches));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("INVALID SPLIT.");
        }
        return 0;
    }

    public void spawn(File[] files){
        int numFiles = to-from;
        ForkJoinSearcherSource left = new ForkJoinSearcherSource(files, from, from+numFiles/2, taskData, subject);
        ForkJoinSearcherSource right = new ForkJoinSearcherSource(files, from+numFiles/2, to, taskData, subject);
        left.fork();
        right.fork();
    }
}

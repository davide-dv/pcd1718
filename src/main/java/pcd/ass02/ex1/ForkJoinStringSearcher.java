package pcd.ass02.ex1;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;

import pcd.ass02.utils.Utils;

public class ForkJoinStringSearcher extends RecursiveTask<Integer> {
    private Pattern pattern;
    private String text;

    public ForkJoinStringSearcher(String text, Pattern pattern){
        this.text = text;
        this.pattern = pattern;
    }

    @Override
    protected Integer compute() {
        return Utils.numMatches(pattern, text);
    }
}

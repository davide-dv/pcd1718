package pcd.ass02.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static List<File> getFiles(String directoryName, int depth) {
        File directory = new File(directoryName);

        List<File> resultList = new ArrayList<File>();

        if(depth>0) {
            File[] fList = directory.listFiles();
            for (File file : fList) {
                if(file.isFile()){
                    resultList.add(file);
                } else if (file.isDirectory()) {
                    resultList.addAll(getFiles(file.getAbsolutePath(), depth-1));
                }
            }
        }

        return resultList;
    }

    public static int numMatches(Pattern p, String s){
        int lastEnd = -1;
        int numMatches = -1;
        do {
            numMatches++;
            Matcher m = p.matcher(s);
            if(s.length()>lastEnd && m.find(lastEnd+1))
                lastEnd = m.end();
            else lastEnd = -1;
        } while(lastEnd!=-1);
        return numMatches;
    }

    public static <T> CompletableFuture<T> makeCompletableFuture(Future<T> future) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return future.get();
            } catch (InterruptedException|ExecutionException e) {
                throw new RuntimeException(e);
            }
        });
    }
}

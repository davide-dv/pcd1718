package pcd.ass02.utils;

import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;

public class RxUtils {
    public static <T> void completionOnLimit(FlowableProcessor<T> subject, int limit){
        Flowable<Integer> nums = Flowable.range(1, Integer.MAX_VALUE);
        subject.zipWith(nums, (x,k) -> k).filter(x -> x==limit).subscribe(x -> subject.onComplete());
    }
}

package pcd.ass02.utils;

public class Box<T> {
    private T value;
    public Box(T init){
        value = init;
    }

    public T get(){ return value; }
    public void set(T value){ this.value = value; }
}

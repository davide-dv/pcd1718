package pcd.ass02.model;

import java.util.regex.Pattern;

public class TaskData {
    protected String basePath;
    protected int depth;
    protected Pattern regex;
    public TaskData(String basePath, int depth, Pattern regex) {
        this.basePath = basePath;
        this.depth = depth;
        this.regex = regex;
    }

    public String getBasePath() {
        return basePath;
    }

    public int getDepth() {
        return depth;
    }

    public Pattern getRegex() {
        return regex;
    }
}

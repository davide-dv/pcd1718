package pcd.ass02.model;

import java.io.File;
import java.util.regex.Pattern;

public class TaskDataFull extends TaskData {
    private File[] files;

    public TaskDataFull(String basePath, int depth, Pattern regex, File[] files) {
        super(basePath, depth, regex);
        this.files = files;
    }

    public File[] getFiles() {
        return files;
    }
}

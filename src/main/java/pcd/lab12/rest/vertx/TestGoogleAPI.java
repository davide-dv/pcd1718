package pcd.lab12.rest.vertx;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class TestGoogleAPI {
	public static void main(String[] args) throws Exception {	
		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient();
		
		long t0 = System.currentTimeMillis();
		client.getNow("maps.googleapis.com","/maps/api/directions/json?origin=Cesena&destination=Bologna&sensor=false", response -> {
			long dt = System.currentTimeMillis() - t0;
			System.out.println("Received response with status code " + response.statusCode() + " in "+dt+"ms");
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				System.out.println("Result: "+obj);
				
				JsonArray routes = obj.getJsonArray("routes");
				routes.forEach( result -> {
					JsonObject route = (JsonObject) result;
					JsonObject bounds = route.getJsonObject("bounds");
					System.out.println("Bounds:");
					System.out.println(bounds.getJsonObject("northeast"));
					System.out.println(bounds.getJsonObject("southwest"));
					
					JsonObject leg = route.getJsonArray("legs").getJsonObject(0);
					long distance = leg.getJsonObject("distance").getLong("value");
					System.out.println("Distance: "+distance);
					
					JsonArray steps = leg.getJsonArray("steps");
					System.out.println("Num steps: "+steps.size());
					
					steps.forEach(elem -> {
						JsonObject step = (JsonObject) elem;
						System.out.println("From: "+step.getJsonObject("start_location"));
						System.out.println("To: "+step.getJsonObject("end_location"));
						System.out.println("--");
					});
				});				
				
				System.exit(0);
			});
		});
	}
}

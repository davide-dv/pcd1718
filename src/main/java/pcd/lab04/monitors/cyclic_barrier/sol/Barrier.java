package pcd.lab04.monitors.cyclic_barrier.sol;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
